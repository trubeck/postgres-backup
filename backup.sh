#!/bin/bash

rm -rf /source

[[ -z "$RESTIC_PASSWORD" ]] && { echo "Restic password not set"; exit 1; }
[[ -z "$PGHOST" ]] && { echo "Postgres host not set"; exit 1; }
[[ -z "$PGUSER" ]] && { echo "Postgres user not set"; exit 1; }
[[ -z "$PGPASSWORD" ]] && { echo "Postgres password not set"; exit 1; }

restic init --repo /target

# Make sure the env variables PGHOST, PGUSER and PGPASSWORD are set
# otherwise this command will probably fail.
# It uses the directory mode, so it creates files for every tables

pg_dump -F d -f /source &&


# Using the force flag is neccesary for incremental backups here, because every execution of pg_dump
# creates new files so restic will otherwise think, these files changed, without actually having different
# contents

restic -r /target backup --force /source &&


# Remove old snapshots

restic -r /target forget --keep-daily 8 --keep-weekly 5 --keep-monthly 13 --keep-yearly 3 --prune