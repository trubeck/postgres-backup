FROM alpine:3.20.0

RUN apk add --no-cache postgresql-client restic bash

COPY backup.sh /etc/periodic/daily/backup

RUN chmod 755 /etc/periodic/daily/backup

CMD ["crond", "-f", "-l", "8"]