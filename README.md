# Postgres Backup

## WARNING

**This code was currently modified and is still untested. Please check for yourself if
it is working as expected**

## Usage

```yaml
version: '2.4'

services:
  postgres_backup:
    image: registry.gitlab.com/trubeck/postgres-backup
    environment:
      RESTIC_PASSWORD: your-restic-password
      PGHOST: your-postgres-host
      PGUSER: your-postgres-password
      PGPASSWORD: your-postgres-host
    volumes:
      - backup-target:/target
    logging:
      options:
        max-size: 10m

volumes:
  backup-target:
    driver_opts:
      type: "nfs4"
      o: "addr=your.nfs.share.ip,nolock,soft,rw"
      device: ":/path/to/your/share"
```